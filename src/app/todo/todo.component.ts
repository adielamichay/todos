import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {TodosService } from '../todos.service';
@Component({
  selector: 'todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})

export class TodoComponent implements OnInit {
  @Input() data:any;
  @Output() myButtonClicked=new EventEmitter<any>();
  
  text;
  key;
  showEditField = false;
  showTheButton = true;
  tempText;
  
  toDelete(){
    this.todosService.deleteTodo(this.key);
  }
  showButton(){
    this.showTheButton = true;
  }
  hideButton(){
    this.showTheButton = false;
  }
  save(){
    this.todosService.update(this.key, this.text);
    this.showEditField = false;
  }
  showEdit(){
    this.showEditField = true;
    this.tempText = this.text;
    this.showTheButton = false;
  }
  cancel(){
    this.showEditField = false;
    this.text = this.tempText
  }

   
  constructor(private todosService:TodosService) { }

  ngOnInit() {
    this.text = this.data.text;
    this.key = this.data.$key;

  }

}
