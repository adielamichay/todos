import { Component, OnInit} from '@angular/core';
import {AngularFireDatabase, AngularFireList} from '@angular/fire/database';
@Component({
  selector: 'userTodos',
  templateUrl: './user-todos.component.html',
  styleUrls: ['./user-todos.component.css']
})
export class UserTodosComponent implements OnInit {

  user = 'jack';
  todoTextFromTodo='No text so far';

  showText($event){
    this.todoTextFromTodo = $event;
  }
  changeuser(){
    this.db.list('/users/'+this.user+'/todos').snapshotChanges().subscribe(
      todos =>{
        this.todos = [];
        todos.forEach(
          todo => {
            let y =todo.payload.toJSON();
            y["$key"] = todo.key;
            this.todos.push(y);
          }
        )
      }
    )

  }

  todos = [ ];

  constructor(private db:AngularFireDatabase) { }

  ngOnInit() {
    this.db.list('/users/'+this.user+'/todos').snapshotChanges().subscribe(
      todos =>{
        this.todos = [];
        todos.forEach(
          todo => {
            let y =todo.payload.toJSON();
            y["$key"] = todo.key;
            this.todos.push(y);
          }
        )
      }
    )
  }
}