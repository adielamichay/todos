import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Observable} from 'rxjs';
import {AngularFireDatabase, AngularFireList} from '@angular/fire/database';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  signup(email:string, password:string){
    return this.firebaseAuth.auth.createUserWithEmailAndPassword(email, password);
  }

  login(email:string, password:string){
    return this.firebaseAuth.auth.signInWithEmailAndPassword(email, password);
  }

  logout(){
    return this.firebaseAuth.auth.signOut();
  }

  updateProfile(user,name:string){
    user.updateProfile({displayName:name, photoURL:''});
  }
//ליוזר ספציפי EndPoint
  addUser(user, name:string){
    let uid = user.uid;
    let ref = this.db.database.ref('/');
    ref.child('users').child(uid).push({'name':name})
  }

  

  user:Observable<firebase.User>;

  constructor(private firebaseAuth:AngularFireAuth, 
    private db:AngularFireDatabase) { 
      this.user = firebaseAuth.authState;
  }
}
