import { Component, OnInit } from '@angular/core';
import{AuthService} from '../auth.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  email:string;
  password:string;
  name:string;
  error= '';
  output= '';


  login(){
    this.authService.login(this.email, this.password)
        .then(user => {
              this.router.navigate(['/']);
             }).catch(err => {
                this.error = err.code;
                this.output = err.message;
                console.log(err);
              })
  }
  constructor(private authService:AuthService,private router:Router) { }

  ngOnInit() {
  }

 

}
